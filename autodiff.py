from __future__ import division
import autograd.numpy as np
from autograd import elementwise_grad
import sys
sys.path.append('/home/yuchaz/codes/random_forest_manifold/')
from data_generator import create_torus_grid, add_noises_on_additional_dimensions
from megaman.geometry import Geometry
import scipy
from megaman.geometry.rmetric import riemann_metric_lazy
import warnings


N, outer_rad, inner_rad, height = 10000, 3, 2, 2
warning_msg = ('autograd_phi has some bug and not yet optmized, '
               'thus should be used with caution. Better to use the '
               'hard coded version.')



# BUG: there is some bugs in the autograd_phi. When at point (0, 5, 0), the
# grad_y(pt) should be (0, -0.2, 0) however it will output nan
def autograd_phi(data, dim=0):
    warnings.warn(warning_msg, FutureWarning)
    def phi(x, y, z):
        return np.arctan(y / x)
    x, y, z = data[:, 0], data[:, 1], data[:, 2]
    fgrad = elementwise_grad(phi, dim)
    grad_ = np.zeros(x.shape)
    y_nonzero = y != 0
    grad_[y_nonzero] = fgrad(x[y_nonzero], y[y_nonzero], z[y_nonzero])
    return grad_


def autograd_theta(data, dim=0, outer_rad=outer_rad,
                   inner_rad=inner_rad, height=height):
    warnings.warn(warning_msg, FutureWarning)
    def theta_outer(x, y, z):
        return np.arcsin(z / (inner_rad * height))

    def theta_inner(x, y, z):
        return np.pi - np.arcsin(z / (inner_rad * height))
    x, y, z = data[:, 0], data[:, 1], data[:, 2]
    fgrad_outer = elementwise_grad(theta_outer, dim)
    fgrad_inner = elementwise_grad(theta_inner, dim)
    grad_ = np.zeros(x.shape)
    z_outer = np.sqrt(x**2 + y**2) > outer_rad
    z_inner = np.logical_not(z_outer)
    grad_[z_outer] = fgrad_outer(x[z_outer], y[z_outer], z[z_outer])
    grad_[z_inner] = fgrad_inner(x[z_inner], y[z_inner], z[z_inner])
    return grad_


def hard_coded_gradient_phi(data, dim=0):
    x, y = data[:, 0], data[:, 1]
    denum = x**2 + y**2
    if dim == 0:
        return -y / denum
    elif dim == 1:
        return x / denum
    else:
        return np.zeros(x.shape)


def hard_coded_gradient_theta(data, dim=0, outer_rad=outer_rad,
                              inner_rad=inner_rad, height=height):
    x, y, z = data[:, 0], data[:, 1], data[:, 2]
    sqrt_xy = np.sqrt(x**2 + y**2)
    denum_xy_part = height * (sqrt_xy - outer_rad)
    denum = z**2 + denum_xy_part**2
    if dim == 2:
        return denum_xy_part / denum
    elif dim == 0 or dim == 1:
        vused = x if dim == 0 else y
        return - height * z * vused / (denum * sqrt_xy)
    else:
        return np.zeros(x.shape)


def gradient(data, var='theta', outer_rad=outer_rad,
             inner_rad=inner_rad, height=height, hard_coded=True):
    def autograd_theta_(data, dim=0):
        return autograd_theta(data, dim, outer_rad, inner_rad, height)
    def hard_coded_theta_(data, dim=0):
        return hard_coded_gradient_theta(data, dim, outer_rad, inner_rad, height)
    if var == 'theta':
        grad_func = autograd_theta_ if not hard_coded else hard_coded_theta_
    elif var == 'phi':
        grad_func = autograd_phi if not hard_coded else hard_coded_gradient_phi
    elif var in 'xyz' and len(var) == 1:
        out = np.zeros(data.shape)
        idx = 'xyz'.index(var)
        out[:, idx] = 1
        return out
    else:
        raise ValueError()
    gradient = np.vstack([grad_func(data, idx) for idx in range(3)]).T
    gradient = add_noises_on_additional_dimensions(
        gradient, data.shape[1] - gradient.shape[1], sigmas=0)
    return gradient


def get_unprojected_X_mat(N=N, outer_rad=outer_rad, inner_rad=inner_rad,
                          height=height, torus_type='clean', normalize=True,
                          subsample=None, random=True):
    torus_noisy, clean_grid, colordict = create_torus_grid(
        N, outer_rad, inner_rad, height)
    subsample_size = clean_grid.shape[0] // subsample
    colordict['theta'], colordict['phi'] = colordict['phi'], colordict['theta']
    if subsample is None:
        subsample = np.arange(clean_grid.shape[0])
    elif random is not True:
        subsample = np.arange(0, clean_grid.shape[0], subsample)
    else:
        subsample = np.random.choice(clean_grid.shape[0], subsample_size, replace=False)
    data = clean_grid if torus_type == 'clean' else torus_noisy
    torus_dict = dict(noisy=torus_noisy, clean=clean_grid, colordict=colordict)
    column_idx = ['theta', 'phi', 'x', 'y', 'z']
    unprojected_X = np.dstack([
        gradient(data[subsample], var, outer_rad, inner_rad, height)
        for var in column_idx])
    torus_dict['grad_theta'] = gradient(data, 'theta', outer_rad, inner_rad, height)
    torus_dict['grad_phi'] = gradient(data, 'phi', outer_rad, inner_rad, height)
    if normalize:
        unprojected_X = normalize_jacobian(unprojected_X)
    return unprojected_X, column_idx, torus_dict, subsample


def normalize_jacobian(jacobian):
    """
    jacobian: ndarray (n, m, p)
        n is the number of points
        m is the ambient dimension
        p is the number of dictionary functions
    """
    # psum = np.linalg.norm(jacobian, axis=(0, 1))
    psum = np.linalg.norm(jacobian, axis=1).sum(axis=0)
    return jacobian / psum[None, None, :]


def project_tangent_hard_coded(grad_theta, grad_phi):
    grad_theta /= np.linalg.norm(grad_theta, axis=1)[:,None]
    grad_phi /= np.linalg.norm(grad_phi, axis=1)[:,None]
    return np.dstack([grad_theta, grad_phi])
    # return np.dstack([grad_phi, grad_theta])


def project_tangent(X, p, nbr, d):
    Z = (X[nbr, :] - np.dot(p, X[nbr, :]))*p[:, np.newaxis]
    sig = np.dot(Z.transpose(), Z)
    e_vals, e_vecs = np.linalg.eigh(sig)
    j = e_vals.argsort()[::-1]
    # Returns indices that will sort array from greatest to least.
    e_vec = e_vecs[:, j]
    e_vec = e_vec[:, :d]  # Gets d largest eigenvectors.
    return e_vec


def get_projected_X_mat(data, A, unprojected_X, dim=2, subsample=None,
                        grad_theta=None, grad_phi=None):
    Ps, nbrs = compute_nbr_wts(A, np.arange(A.shape[0]))
    if False:
        print('proj')
        if grad_theta is None or grad_phi is None:
            raise ValueError()
        proj_tensor = project_tangent_hard_coded(grad_theta, grad_phi)
    else:
        proj_tensor = np.array([project_tangent(data, p, nbr, d=dim)
                                for p, nbr in zip(Ps, nbrs)])
    return (np.einsum('ijk,ijl->ilk', unprojected_X, proj_tensor[subsample]),
            proj_tensor, nbrs)


def compute_nbr_wts(A, sample, weighted=False):
    Ps = list()
    nbrs = list()
    for ii in range(len(sample)):
        Ai = A[sample[ii], :]
        if not weighted:
            p = np.zeros(A.shape[1])
            nbr_idx = Ai.nonzero()[1]
            p[nbr_idx] = 1 / Ai.nnz
            nbrs.append(nbr_idx)
            Ps.append(p[nbr_idx])
        else:
            w = np.array(Ai.todense()).flatten()
            p = w / np.sum(w)
            nbrs.append(np.where(p > 0)[0])
            Ps.append(p[nbrs[ii]])
    return Ps, nbrs


def prepare_X_train(N=N, outer_rad=outer_rad, inner_rad=inner_rad,
                    height=height, torus_type='clean', bandwidth=None,
                    dim=2, subsample=1):
    uproj_X, column_idx, torus_dict, subsample = get_unprojected_X_mat(
        N, outer_rad, inner_rad, height, torus_type, subsample=subsample)
    used_data = torus_dict[torus_type]
    print('ambient dimension is {}; intrinsic dimension is {}'.format(
        used_data.shape[1], dim))
    if bandwidth is None and torus_type == 'clean':
        bandwidth = 0.5
    elif bandwidth is None and torus_type == 'noisy':
        bandwidth = 1.2
    rad = bandwidth * 3
    adjacency_kwds = dict(radius=rad)
    adjacency_method = 'cyflann'
    cyflann_kwds = dict(index_type='kdtrees',
                        num_trees=10, num_checks=60)
    adjacency_kwds['cyflann_kwds'] = cyflann_kwds
    affinity_method = 'gaussian'
    affinity_kwds = dict(radius=bandwidth)
    laplacian_method='geometric'
    laplacian_kwds = dict(scaling_epps=bandwidth)
    geom = Geometry(
        adjacency_method=adjacency_method, adjacency_kwds=adjacency_kwds,
        affinity_method=affinity_method, affinity_kwds=affinity_kwds,
        laplacian_method=laplacian_method, laplacian_kwds=laplacian_kwds)
    geom.set_data_matrix(used_data)
    A = geom.compute_affinity_matrix()
    proj_X, proj_tensor, nbrs = get_projected_X_mat(
        used_data, A, uproj_X, dim, subsample,
        torus_dict['grad_theta'], torus_dict['grad_phi'])
    torus_dict['used_type'] = torus_type
    return proj_X, column_idx, torus_dict, proj_tensor, nbrs, geom, subsample


def construct_X(jacobian, q, orig_method=False):
    """
    Parameters
    ----------
    jacobian: (N, d, p)
    q: embedding dimension

    Returns
    -------
    xmatq: (N * q, N * d * q)
    groups: groups
    """
    N, d, p = jacobian.shape
    print('Embedding dimension is {}'.format(q))
    xmat = scipy.linalg.block_diag(*jacobian)
    xmatq = scipy.linalg.block_diag(*[xmat for _ in range(q)])
    groups = np.tile(np.arange(p), N * q)
    return xmatq, groups


def construct_diff(data, i, nbr):
    return data[nbr, :] - data[i]


def compute_jacobian_F(local_projection, laplacian, embedding,
                       subsample, nbrs):
    n, q, dim = subsample.shape[0], embedding.shape[1], local_projection.shape[1]
    dF = np.zeros((n, dim, q))
    H = riemann_metric_lazy(embedding, subsample, laplacian, q)[0]
    G = compute_G_from_H(H, mdimG=dim)
    for ii, idx, Gi in zip(range(n), subsample, G):
        nbr = nbrs[idx]
        AiT = construct_diff(local_projection, idx, nbr)
        BiT = construct_diff(embedding, idx, nbr)
        dF[ii, ...] = np.linalg.multi_dot([np.linalg.pinv(AiT), BiT, Gi])
    return dF


def compute_G_from_H(H, mdimG=None):
    n_samples = H.shape[0]
    n_dim = H.shape[2]
    if mdimG is None:
        mdimG = n_dim
    Huu, Hsvals, Hvv = np.linalg.svd(H)
    Gsvals = 1./Hsvals
    G = np.zeros((n_samples, n_dim, n_dim))
    for i in np.arange(n_samples):
        Hiuu, Hval, Hivv = np.linalg.svd(H[i])
        G[i,:,:] = np.dot(Hiuu[:, :mdimG],
                          np.dot(np.diag(1/Hval[:mdimG]), Hivv[:mdimG, :]))
    return G


def construct_Y(jacobian_F):
    """
    Parameters
    ----------
    jacobian_F: size (N, d, q)

    Returns
    -------
    y_train: (Flattened) jacobian_F, size (q N d, )
    """
    return np.rollaxis(jacobian_F, -1).flatten()
